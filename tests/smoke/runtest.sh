#!/bin/bash

# If one of the commands below returns non-zero then exit immediately
set -e

echo "m17n-db version: `m17n-db --version`"
echo "m17n-db directory: `m17n-db`"
echo " "
echo " "
echo "Test minimal syntax presence for mim files"
for file in `ls /usr/share/m17n/*.mim`; do
    grep "(input-method" $file
    if [ $? == 0 ]; then
        echo "PASSED: minimal syntax for $file"
    else
        echo "FAILED: minimal syntax for $file"
        exit 1
    fi
done

